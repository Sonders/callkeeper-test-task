let buildRoute = require('./route_builder.js')

let cards = [
	{
		previousStage: null,
		currentStage: 'Gerona Airport',
		nextStage: 'Stockholm',
		transport: 'flight SK455',
		gate: '45B',
		seat: '3A',
		comment: 'Baggage drop at ticket counter 344'
	},
	{
		previousStage: null,
		currentStage: 'Barselona',
		nextStage: 'Gerona Airport',
		transport: 'the airport bus',
		gate: null,
		seat: null,
		comment: 'No seat assignment'
	},	
	
	{
		previousStage: null,
		currentStage: 'New York',
		nextStage: null,
		transport: null,
		gate: null,
		seat: null,
		comment: null
	},
	{
		previousStage: null,
		currentStage: 'Stockholm',
		nextStage: 'New York',
		transport: 'flight SK22',
		gate: '22',
		seat: '7B',
		comment: 'Baggage will be automatically transferred from your last leg'
	},
	
	{
		previousStage: null,
		currentStage: 'Madrid',
		nextStage: 'Barselona',
		transport: 'train 78A',
		gate: null,
		seat: '45B',
		comment: null
	},
]

function getRouteDescription(cards) {
	let routeCards = buildRoute(cards)

	routeCards.forEach((card, index) => {
		if (card.nextStage !== null) {
			let description = `${index + 1}. Take ${card.transport} from ${card.currentStage} to ${card.nextStage}.`

			if (card.gate !== null) {
				description += `Gate ${card.gate}.`
			}

			if (card.seat !== null) {
				description += `Seat ${card.seat}.`
			}

			if (card.comment !== null) {
				description += `${card.comment}.`
			}

			console.log(description)
		}
	})
}

getRouteDescription(cards)
