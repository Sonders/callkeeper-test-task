/*
	Input: list of
		{
			previousStage: string,
			currentStage: string,
			nextStage: string,
			transport: string,
			gate: string,
			seat: string,
			comment: string
		}

	Output: sorted list
*/

function buildRoute(cards) {
	cards.map(card => {
		card.index = 1
	})

	cards = cards.map((card, index) => {
		let currentStage = card.currentStage

		let previousCard = cards.find((element, index) => {
			return element.nextStage == currentStage
		})

		if (previousCard !== undefined) {
			card.previousStage = previousCard.currentStage
		}

		return card
	})

	let firstCardIndex = cards.findIndex((element, index) => {
		return element.previousStage == null
	})

	cards[firstCardIndex].index = 0
	cards = sortCards(cards)

	let index = 0

	for (let i = 0; i < cards.length; i++) {
		card = cards[index]

		let nextCardIndex = cards.findIndex((element, index) => {
			return card.nextStage === element.currentStage
		})

		if (nextCardIndex !== -1) {
			cards[nextCardIndex].index = card.index + 1

			index = nextCardIndex
		}
	}

	sorted = sortCards(cards)

	return sorted
}

function sortCards(cards) {
	let sorted = cards.sort((first, second) => {
		if (first.index > second.index) 
			return 1

		if (first.index < second.index)
			return -1

		return 0
	})

	return sorted
}

module.exports = getRouteDescription